
public class StringCompareTo {
	public static void main(String[] args) {

			       String str1 = "ABCD";
			       String str2 = "abc";
			       String str3 = "ABCD";

			       int var1 = str1.compareTo( str2 );
		
			       System.out.println("str1 & str2 comparison: "+var1);

			       int var2 = str1.compareTo( str3 );
			       System.out.println("str1 & str3 comparison: "+var2);

			       int var3 = str2.compareTo("ABCD");
			       System.out.println("str2 & string argument comparison: "+var3);
			 

	}

}

//0=>s1=s2
//+=>s1>s2
//-=>S1<s2

public class StringEndsWith {
		   public static void main(String[] args) {  
			   
			   	String str1 = "Learn java string";
			    String str2 = new String("at BCAS");
			
				if(str1.endsWith("string")) {
					System.out.println("The Given String ends with string");
				}
				
			  
			       boolean var1 = str1.endsWith("String");
			       boolean var2 = str1.endsWith("string");
			       boolean var3 = str2.endsWith("BCAS");
			       boolean var4 = str2.endsWith("ABC");
			       
			       System.out.println("str1 ends with String: "+ var1);
			       System.out.println("str1 ends with string: "+ var2);
			       System.out.println("str2 ends with BCAS: "+ var3);
			       System.out.println("str2 ends with ABC: "+ var4);
			   
		   }  
		
}

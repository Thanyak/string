
public class StringStartsWith {

	public static void main(String[] args) {
	
				//given string
				String s = "This is the best way";  
					
				//checking whether the given string starts with "This"
				System.out.println(s.startsWith("This"));  
					
				//checking whether the given string starts with "It"
				System.out.println(s.startsWith("It"));  
				
				// check after using index, the given string start with"is"/"This"
			    System.out.println("substring of str(starting from 6th index) has is prefix: "
			    		   +s.startsWith("is", 5));
			       
			    System.out.println("substring of str(starting from 6th index) has the prefix: "
			    		   +s.startsWith("This", 5));
			 

	}

}

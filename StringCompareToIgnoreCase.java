
public class StringCompareToIgnoreCase {

		   public static void main(String args[]) {
			String string1 = "BCAS";
			String string2 = "bcas";
			String string3 = "BCAS";

			int var1 = string1.compareToIgnoreCase(string2);
			System.out.println("string1 and string2 comparison: "+var1);

			int var2 = string1.compareToIgnoreCase(string3);
			System.out.println("string1 and string3 comparison: "+var2);

			int var3 = string1.compareToIgnoreCase("BcAs");
			System.out.println("string1 and HeLLo comparison: "+var3);
		   }
		
}
